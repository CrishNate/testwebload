// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TestWebLoadGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class TESTWEBLOAD_API ATestWebLoadGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

	
public:	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
    class UTeamControllerComponent* TeamControllerComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
    class ULogoControllerComponent* LogoControllerComponent;


private:
	ATestWebLoadGameModeBase(const FObjectInitializer& ObjectInitializer);
};

#define TestWebLoadGameMode_Verified(WorldContextObject) \
ATestWebLoadGameModeBase* TestWebLoadGameMode = Cast<ATestWebLoadGameModeBase>(UGameplayStatics::GetGameMode(WorldContextObject)); \
verify(IsValid(TestWebLoadGameMode));