﻿#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "Engine/Texture2D.h"
#include "IImageWrapper.h"
#include "ImageHandler.generated.h"

UENUM(BlueprintType)
enum class EJoyImageFormats : uint8
{
    JPG		UMETA(DisplayName = "JPG"),
    PNG		UMETA(DisplayName = "PNG"),
    BMP		UMETA(DisplayName = "BMP"),
    ICO		UMETA(DisplayName = "ICO"),
    EXR		UMETA(DisplayName = "EXR"),
    ICNS	UMETA(DisplayName = "ICNS")
};

UCLASS()
class TESTWEBLOAD_API UImageHandler : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

    
public:
    static UTexture2D* LoadTexture2DFromFile(const FString& FileName, EJoyImageFormats ImageFormat, TArray<uint8> RawFileData, bool& IsValid);


private:
    static FString GetJoyImageExtension(EJoyImageFormats JoyFormat);
    static EImageFormat GetJoyImageFormat(EJoyImageFormats JoyFormat);

};
