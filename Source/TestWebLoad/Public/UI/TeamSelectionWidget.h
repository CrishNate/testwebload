// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "TeamSelectionWidget.generated.h"



UCLASS()
class TESTWEBLOAD_API UTeamSelectionWidget : public UUserWidget
{
	GENERATED_BODY()

	
public:
	UTeamSelectionWidget(const FObjectInitializer& ObjectInitializer);


public:
	UFUNCTION(BlueprintCallable)
	void ExecuteTeamChange(FName TeamTag);
};
