﻿#pragma once

#include "Engine/Texture2D.h"
#include "Data/TeamInfo.h"
#include "TeamVizualInfo.generated.h"


UCLASS(BlueprintType)
class TESTWEBLOAD_API UTeamVizualInfo : public UObject
{
	GENERATED_BODY()


public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Team")
	FName Name;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Team")
    class UStaticMesh* StaticMesh;


public:
	UFUNCTION(BlueprintPure, Category = "Team Vizual Info")
    static UTeamVizualInfo* GetTeamVizualInfo(FName TeamTag, const FTeamInfo& TeamInfo);

	UFUNCTION(BlueprintPure)
	UTexture2D* GetTexture();
	
	UFUNCTION()
    void OnDownloadComplete(FString Filename);


public:
	bool IsFinished();


public:
	class UWebImageDownloader* WebImageDownloader;


private:
	bool bIsFinished;
};
