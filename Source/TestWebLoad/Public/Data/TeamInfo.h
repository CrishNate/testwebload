﻿#pragma once

#include "Engine/DataTable.h"

#include "TeamInfo.generated.h"


USTRUCT(BlueprintType)
struct FTeamInfo : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Team")
    FName Name;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Team")
	FString ImageLink;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Team")
    class UStaticMesh* StaticMesh;
	
	FTeamInfo()
		: StaticMesh(nullptr)
	{ }
};