﻿#pragma once

#include "UObject/Interface.h"
#include "ILogoLevelInterface.generated.h"



UINTERFACE(Blueprintable)
class TESTWEBLOAD_API UILogoLevelInterface : public UInterface
{
    GENERATED_BODY()
};


class TESTWEBLOAD_API IILogoLevelInterface
{
    GENERATED_BODY()


public:
    UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "LogoLevelInterface")
    TArray<class ATargetPoint*> GetLogoSpawnPoints() const;
};