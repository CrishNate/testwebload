// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TeamLogo.generated.h"

UCLASS()
class TESTWEBLOAD_API ATeamLogo : public AActor
{
	GENERATED_BODY()

	
public:	
	ATeamLogo();


public:
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "Components")
    class UStaticMeshComponent* MeshComponent;
	
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "Components")
    class UDecalComponent* DecalComponent;

	UPROPERTY(BlueprintReadOnly, Category = "Materials")
    UMaterialInstanceDynamic* DynamicDecalMaterial;

	
public:
	UFUNCTION()
	void ChangeTeamInfo(FName TeamTag, class UTeamVizualInfo* TeamInfo);


protected:
	void BeginPlay() override;
};
