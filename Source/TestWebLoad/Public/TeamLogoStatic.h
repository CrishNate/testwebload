// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TeamLogo.h"
#include "TeamLogoStatic.generated.h"

/**
 * 
 */
UCLASS()
class TESTWEBLOAD_API ATeamLogoStatic : public ATeamLogo
{
	GENERATED_BODY()
	
	
protected:
    virtual void BeginPlay() override;
};
