// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"

#include "LogoControllerComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TESTWEBLOAD_API ULogoControllerComponent : public UActorComponent
{
	GENERATED_BODY()

	
public:	
	ULogoControllerComponent();


public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Class")
	TSubclassOf<class ATeamLogo> TeamLogoClass;
	

protected:
	void BeginPlay() override;
	

public:
	UFUNCTION(BlueprintPure, Category = "Level Interface", meta = (WorldContext = "WorldContextObject"))
	TScriptInterface<class IILogoLevelInterface> GetLogoLevelInterface(const UObject* WorldContextObject);


public:
	UFUNCTION()
	void CreateLogos(FName TeamTag, class UTeamVizualInfo* TeamVizualInfo);
	
	class ATeamLogo* CreateLogo(FName TeamTag, class UTeamVizualInfo* TeamVizualInfo, FTransform Transform);


public:
	TArray<class ATeamLogo*> Logos;
};
