// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Data/TeamInfo.h"
#include "Data/TeamVizualInfo.h"
#include "Components/ActorComponent.h"
#include "GameFramework/Actor.h"

#include "TeamControllerComponent.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FDelegate_OnTeamSwitch, FName, TeamTag, UTeamVizualInfo*, TeamVizualInfo);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FDelegate_OnDownloadComlete);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TESTWEBLOAD_API UTeamControllerComponent : public UActorComponent
{
	GENERATED_BODY()

	
public:	
	UTeamControllerComponent();


public:
	UPROPERTY(BlueprintAssignable)
	FDelegate_OnDownloadComlete OnDownloadComlete;

	
protected:
    UPROPERTY(EditDefaultsOnly, Category = "Items")
    class UDataTable* TeamInfos;

	UPROPERTY(BlueprintReadOnly)
    TMap<FName, UTeamVizualInfo*> TeamInfosMap;

	
public:
	UFUNCTION(BlueprintPure)
    FORCEINLINE TMap<FName, UTeamVizualInfo*> GetAllTeams() { return TeamInfosMap; }

	UFUNCTION(BlueprintPure, BlueprintAuthorityOnly, Category = "RGShooter|GameMode", meta = (WorldContext = "WorldContextObject"))
    static UTeamControllerComponent* GetTeamControllerComponent(const UObject* WorldContextObject);

	UFUNCTION()
	void CheckIsAllLoaded(FString FileName);

	UFUNCTION(BlueprintCallable)
    void SwitchTeam(FName TeamTag);

	
public:
	virtual void BeginPlay() override;
		
	bool IsDownloadComplete();
	
	void LoadData();


public:
	FDelegate_OnTeamSwitch OnTeamSwitch;
};
