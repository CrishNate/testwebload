﻿#pragma once

#include "Runtime/Online/HTTP/Public/Http.h"
#include "Runtime/Online/HTTP/Public/HttpManager.h"
#include "Engine/Texture2D.h"
#include "WebImageDownloader.generated.h"

DECLARE_DELEGATE_OneParam(FDirWebImageRecursiveDownloader, FString);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FWebImageDownloader_OnDownloadComplete, FString, FileName);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FWebImageDownloader_OnDownloadError, FString, FileName);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FWebImageDownloader_OnDownloadProgress, FString, FileName, int32, progress);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FWebImageDownloader_OnUpdateCheckCompleted, FString, FileName, bool, hasUpdate);

UCLASS(Blueprintable, BlueprintType)
class TESTWEBLOAD_API UWebImageDownloader : public UObject
{
	GENERATED_BODY()

	
public:
	UFUNCTION(BlueprintPure, Category = "WebImage Downloader", Meta = (DisplayName = "Get A Web Image Downloader"))
	static UWebImageDownloader* GetWebImageDownloader(FString WebImageName, FString URL, bool& IsValid);

	UFUNCTION(BlueprintPure, Category = "WebImage Downloader", Meta = (DisplayName = "Downloaded Web Images List"))
	static TArray<FString> DownloadedWebImagesList();

	UFUNCTION(BlueprintPure, Category = "WebImage Downloader", Meta = (DisplayName = "Delete Web Image File"))
	static void DeleteWebImageFile(FString WebImageName, bool &isDeleted);

	UFUNCTION(BlueprintPure, Category = "WebImage Downloader", Meta = (DisplayName = "Is Web Image Already Downloaded"))
	static bool IsWebImageDownloaded(FString WebImageName);

	UPROPERTY(BlueprintAssignable, Category = "WebImage Downloader")
	FWebImageDownloader_OnDownloadComplete OnWebImageDownloadCompleted;

	UPROPERTY(BlueprintAssignable, Category = "WebImage Downloader")
	FWebImageDownloader_OnDownloadError OnWebImageDownloadError;

	UPROPERTY(BlueprintAssignable, Category = "WebImage Downloader")
	FWebImageDownloader_OnDownloadProgress OnWebImageDownloadProgress;

	UPROPERTY(BlueprintAssignable, Category = "WebImage Downloader")
	FWebImageDownloader_OnUpdateCheckCompleted OnUpdateCheckCompleted;

	UFUNCTION(BlueprintCallable, Category = "WebImage Downloader", Meta = (DisplayName = "Does Web Image Have Update"))
	void CheckIfWebImageHasUpdate();

	UFUNCTION(BlueprintCallable, Category = "WebImage Downloader", Meta = (DisplayName = "Download Web Image"))
	void DownloadWebImage();

	UFUNCTION(BlueprintCallable, Category = "WebImage Downloader", Meta = (DisplayName = "Cancel Download"))
	void CancelDownload();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WebImage Downloader", Meta = (DisplayName = "Web Image Name"))
	FString OriginalWebImageName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WebImage Downloader", Meta = (DisplayName = "Server URL"))
	FString OriginalURL;

	static FString WebImageFolder();
	static bool CreateWebImageFolder();
	FORCEINLINE UTexture2D* GetTexture() const { return Texture2D; }
	
	
private:
	void HttpRequestComplete(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);
	void HttpDownloadComplete(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);
	void HttpRequestProgress(FHttpRequestPtr Request, int32 bytesSent, int32 bytesReceived);

	void UpdateCheckHttpRequestComplete(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);

	int32 RequestSize;
	FString RequestUrl;

	TSharedPtr<IHttpRequest> UpdateRequest;
	TSharedPtr<IHttpRequest> DownloadRequest;

	UTexture2D* Texture2D;
}; 