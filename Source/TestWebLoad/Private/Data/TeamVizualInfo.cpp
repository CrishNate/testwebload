﻿#include "Data/TeamVizualInfo.h"
#include "WebImageDownloader.h"


UTeamVizualInfo* UTeamVizualInfo::GetTeamVizualInfo(FName TeamTag, const FTeamInfo& TeamInfo)
{
    auto NewTeamVizualInfo = NewObject<UTeamVizualInfo>();
    NewTeamVizualInfo->Name = TeamInfo.Name;
    NewTeamVizualInfo->StaticMesh = TeamInfo.StaticMesh;

    if (!NewTeamVizualInfo)
    {
        UE_LOG(LogTemp, Error, TEXT("[UTeamVizualInfo::GetTeamVizualInfo] Could not be created"));
        return nullptr;
    }
    
    bool bValid;
    auto WebImageDownloader = UWebImageDownloader::GetWebImageDownloader(TeamTag.ToString(), TeamInfo.ImageLink, bValid);
    NewTeamVizualInfo->WebImageDownloader = WebImageDownloader;
    WebImageDownloader->DownloadWebImage();
                
    if (bValid)
        WebImageDownloader->OnWebImageDownloadCompleted.AddDynamic(NewTeamVizualInfo, &UTeamVizualInfo::OnDownloadComplete);

    return NewTeamVizualInfo;
}

UTexture2D* UTeamVizualInfo::GetTexture()
{
    return WebImageDownloader->GetTexture();
}

void UTeamVizualInfo::OnDownloadComplete(FString Filename)
{
    bIsFinished = true;
}

bool UTeamVizualInfo::IsFinished()
{
    return bIsFinished;
}