// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/TeamSelectionWidget.h"
#include "TestWebLoadGameModeBase.h"
#include "Components/TeamControllerComponent.h"


UTeamSelectionWidget::UTeamSelectionWidget(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
}

void UTeamSelectionWidget::ExecuteTeamChange(FName TeamTag)
{
    Cast<ATestWebLoadGameModeBase>(GetWorld()->GetAuthGameMode())->TeamControllerComponent->SwitchTeam(TeamTag);
}
