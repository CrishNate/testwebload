// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "TestWebLoadGameModeBase.h"
#include "Components/TeamControllerComponent.h"
#include "Components/LogoControllerComponent.h"


ATestWebLoadGameModeBase::ATestWebLoadGameModeBase(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	TeamControllerComponent = CreateDefaultSubobject<UTeamControllerComponent>(TEXT("TeamControllerComponent"));
	LogoControllerComponent = CreateDefaultSubobject<ULogoControllerComponent>(TEXT("LogoControllerComponent"));
}
