// Fill out your copyright notice in the Description page of Project Settings.


#include "TeamLogo.h"
#include "Components/StaticMeshComponent.h"
#include "Components/TeamControllerComponent.h"
#include "Components/DecalComponent.h"
#include "Materials/MaterialInstanceDynamic.h"


ATeamLogo::ATeamLogo()
{
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BackpackComponent"));
	SetRootComponent(MeshComponent);

	DecalComponent = CreateDefaultSubobject<UDecalComponent>(TEXT("DecalComponent"));
	DecalComponent->SetupAttachment(MeshComponent);
}

void ATeamLogo::BeginPlay()
{
	Super::BeginPlay();

	DynamicDecalMaterial = UMaterialInstanceDynamic::Create(DecalComponent->GetDecalMaterial(), nullptr);
	DecalComponent->SetDecalMaterial(DynamicDecalMaterial);
}

void ATeamLogo::ChangeTeamInfo(FName TeamTag, UTeamVizualInfo* TeamInfo)
{
	MeshComponent->SetStaticMesh(TeamInfo->StaticMesh);
	DynamicDecalMaterial->SetTextureParameterValue("Texture", TeamInfo->GetTexture());
}
