// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/TeamControllerComponent.h"
#include "Data/TeamVizualInfo.h"
#include "TestWebLoadGameModeBase.h"
#include "WebImageDownloader.h"

#include "Kismet/GameplayStatics.h"


UTeamControllerComponent::UTeamControllerComponent()
{
}

void UTeamControllerComponent::LoadData()
{
    if (TeamInfos)
    {
        for (auto& TeamTag : TeamInfos->GetRowNames())
        {
            auto TeamInfo = TeamInfos->FindRow<FTeamInfo>(TeamTag, "TeamControllerComponent");
            if (TeamInfo)
            {
	            UTeamVizualInfo* Object = UTeamVizualInfo::GetTeamVizualInfo(TeamTag, *TeamInfo);
                Object->WebImageDownloader->OnWebImageDownloadCompleted.AddDynamic(this, &UTeamControllerComponent::CheckIsAllLoaded);
                TeamInfosMap.Add(TeamTag, Object);
            }
        }
    }
}

void UTeamControllerComponent::SwitchTeam(FName TeamTag)
{
    if (TeamInfosMap.Contains(TeamTag))
    {
        OnTeamSwitch.Broadcast(TeamTag, TeamInfosMap[TeamTag]);
    }
    else
    {
		UE_LOG(LogTemp, Error, TEXT("[TeamControllerComponent::SwitchTeam] Doesn't have TeamTag %s"), *TeamTag.ToString());
    }
}

void UTeamControllerComponent::CheckIsAllLoaded(FString FileName)
{
    if (IsDownloadComplete())
    {
        OnDownloadComlete.Broadcast();
    }
}

UTeamControllerComponent* UTeamControllerComponent::GetTeamControllerComponent(const UObject* WorldContextObject)
{
    TestWebLoadGameMode_Verified(WorldContextObject)
    return TestWebLoadGameMode->TeamControllerComponent;
}

void UTeamControllerComponent::BeginPlay()
{
    Super::BeginPlay();
    
    LoadData();
}

bool UTeamControllerComponent::IsDownloadComplete()
{
    TArray<FName> Keys;
    TeamInfosMap.GetKeys(Keys);
    for (FName Key : Keys)
    {
        if (!TeamInfosMap[Key]->IsFinished())
        {
            return false;
        }
    }
    
    return true;
}
