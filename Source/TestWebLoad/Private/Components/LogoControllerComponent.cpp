// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/LogoControllerComponent.h"
#include "Components/TeamControllerComponent.h"
#include "Interface/ILogoLevelInterface.h"
#include "Data/TeamVizualInfo.h"
#include "TeamLogo.h"

#include "Engine/Engine.h"
#include "Engine/TargetPoint.h"
#include "Engine/LevelScriptActor.h"
#include "Kismet/KismetSystemLibrary.h"


ULogoControllerComponent::ULogoControllerComponent()
{
}

void ULogoControllerComponent::BeginPlay()
{
    Super::BeginPlay();

	UTeamControllerComponent::GetTeamControllerComponent(this)->OnTeamSwitch.AddDynamic(this, &ULogoControllerComponent::CreateLogos);
}

TScriptInterface<IILogoLevelInterface> ULogoControllerComponent::GetLogoLevelInterface(const UObject* WorldContextObject)
{
    UWorld* World = GEngine->GetWorldFromContextObject(WorldContextObject, EGetWorldErrorMode::ReturnNull);
    if (!IsValid(World))
        return nullptr;

    ULevel* Level = World->GetLevel(0);
    if (!IsValid(Level))
        return nullptr;

    AActor* LevelScriptActor = Level->GetLevelScriptActor();
    if (!IsValid(LevelScriptActor))
        return nullptr;

    if (UKismetSystemLibrary::DoesImplementInterface(LevelScriptActor, UILogoLevelInterface::StaticClass()))
        return LevelScriptActor;

    return nullptr;
}

void ULogoControllerComponent::CreateLogos(FName TeamTag, UTeamVizualInfo* TeamVizualInfo)
{
    TScriptInterface<IILogoLevelInterface> LogoLevelInterface = GetLogoLevelInterface(GetWorld());

    for (ATeamLogo* Logo : Logos)
    {
        Logo->Destroy();
    }

    Logos.Empty();

    for (ATargetPoint* Point : IILogoLevelInterface::Execute_GetLogoSpawnPoints(LogoLevelInterface.GetObject()))
    {
        CreateLogo(TeamTag, TeamVizualInfo, Point->GetTransform());
    }
}
	
class ATeamLogo* ULogoControllerComponent::CreateLogo(FName TeamTag, UTeamVizualInfo* TeamVizualInfo, FTransform Transform)
{
    FActorSpawnParameters ASP;
    ASP.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
    auto TeamLogo = GetWorld()->SpawnActor<ATeamLogo>(TeamLogoClass, Transform, ASP);
    if (IsValid(TeamLogo))
    {
        TeamLogo->ChangeTeamInfo(TeamTag, TeamVizualInfo);
        Logos.Add(TeamLogo);
    }
    else
    {
        UE_LOG(LogTemp, Error, TEXT("[ULogoControllerComponent:CreateLogo] No logo created"))
        return nullptr;
    }

    return TeamLogo;
}
