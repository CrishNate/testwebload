﻿#include "ImageHandler.h"

#include "IImageWrapperModule.h"
#include "Modules/ModuleManager.h"


EImageFormat UImageHandler::GetJoyImageFormat(EJoyImageFormats JoyFormat)
{
	switch (JoyFormat)
	{
		case EJoyImageFormats::JPG: return EImageFormat::JPEG;
		case EJoyImageFormats::PNG: return EImageFormat::PNG;
		case EJoyImageFormats::BMP: return EImageFormat::BMP;
		case EJoyImageFormats::ICO: return EImageFormat::ICO;
		case EJoyImageFormats::EXR: return EImageFormat::EXR;
		case EJoyImageFormats::ICNS: return EImageFormat::ICNS;
	}

	return EImageFormat::JPEG;
}

FString UImageHandler::GetJoyImageExtension(EJoyImageFormats JoyFormat)
{
	switch (JoyFormat)
	{
		case EJoyImageFormats::JPG: return ".jpg";
		case EJoyImageFormats::PNG: return ".png";
		case EJoyImageFormats::BMP: return ".bmp";
		case EJoyImageFormats::ICO: return ".ico";
		case EJoyImageFormats::EXR: return ".exr";
		case EJoyImageFormats::ICNS: return ".icns";
	}
	return ".png";
}

UTexture2D* UImageHandler::LoadTexture2DFromFile(const FString& FileName, EJoyImageFormats ImageFormat, TArray<uint8> RawFileData, bool& IsValid)
{
	IsValid = false;
	UTexture2D* LoadedT2D = NULL;

	IImageWrapperModule& ImageWrapperModule = FModuleManager::LoadModuleChecked<IImageWrapperModule>(FName("ImageWrapper"));
	IImageWrapperPtr ImageWrapper = ImageWrapperModule.CreateImageWrapper(GetJoyImageFormat(ImageFormat));

	//Create T2D!
	if (ImageWrapper.IsValid() && ImageWrapper->SetCompressed(RawFileData.GetData(), RawFileData.Num()))
	{
		const TArray<uint8>* UncompressedBGRA = NULL;
		if (ImageWrapper->GetRaw(ERGBFormat::BGRA, 8, UncompressedBGRA))
		{
			LoadedT2D = UTexture2D::CreateTransient(ImageWrapper->GetWidth(), ImageWrapper->GetHeight(), PF_B8G8R8A8);

			if (!LoadedT2D)
				return nullptr;

			//Copy!
			void* TextureData = LoadedT2D->PlatformData->Mips[0].BulkData.Lock(LOCK_READ_WRITE);
			FMemory::Memcpy(TextureData, UncompressedBGRA->GetData(), UncompressedBGRA->Num());
			LoadedT2D->PlatformData->Mips[0].BulkData.Unlock();

			//Update!
			LoadedT2D->UpdateResource();
		}
	}

	// Success!
	IsValid = true;
	return LoadedT2D;
}