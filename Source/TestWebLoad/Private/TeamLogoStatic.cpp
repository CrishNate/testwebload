// Fill out your copyright notice in the Description page of Project Settings.


#include "TeamLogoStatic.h"
#include "Materials/MaterialInstanceDynamic.h"
#include "Components/DecalComponent.h"
#include "Components/TeamControllerComponent.h"



void ATeamLogoStatic::BeginPlay()
{
	Super::BeginPlay();
	
	UTeamControllerComponent::GetTeamControllerComponent(this)->OnTeamSwitch.AddDynamic(this, &ATeamLogo::ChangeTeamInfo);
}